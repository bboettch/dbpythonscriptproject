#!/usr/bin/env python

import psycopg2                    # database work in python uses psycopg2 library to connect to postgres database
from psycopg2 import Error
import pandas.io.sql as psql
import random
import string
from faker import Faker
import time


def create_connection():            #establishes connection with "healthtest" database
    conn = None
    try:
        conn = psycopg2.connect(    #parameters to create connection
            user = "Brandon",
            host = "localhost",
            port = "5432",
            database = "healthtest")
        return conn
    except Error as e:              #if no connection throw error
        print(e)
    
    return conn                     # returns connection to be used for command executions

def create_tables():            #creates owners, pet tables

    commands = (                #creates the SQL commands to be used
        """
        CREATE TABLE insurance (
            insurance_id SERIAL PRIMARY KEY,
            policy_number int,
            provider VARCHAR(100),
            phone VARCHAR(100)
        )
        """,
        """
        CREATE TABLE owners (
            owner_id SERIAL PRIMARY KEY,
            insurance_id int REFERENCES insurance (insurance_id),
            name VARCHAR(100) NOT NULL,
            phone VARCHAR(50),
            address VARCHAR(100)
        )
        """,
        """ 
        CREATE TABLE pet (
            pet_id SERIAL PRIMARY KEY,
            owner_id int REFERENCES owners (owner_id) NOT NULL,
            name VARCHAR(100),
            age int,
            weight VARCHAR(20),
            species VARCHAR(100)
        )
        """,
        """
        CREATE TABLE doctor (
            doctor_id SERIAL PRIMARY KEY,
            name VARCHAR(100),
            specialization VARCHAR(100),
            phone VARCHAR(100),
            address VARCHAR(150)
        )
        """,
        """
        CREATE TABLE location (
            location_id SERIAL PRIMARY KEY,
            name VARCHAR(100),
            phone VARCHAR(100),
            address VARCHAR(150)
        )
        """,
        """
        CREATE TABLE medication (
            medication_id SERIAL PRIMARY KEY,
            name VARCHAR(100),
            manufactorer VARCHAR(100),
            dose VARCHAR(100),
            prescribed_by VARCHAR(150)
        )
        """,
        """
        CREATE TABLE test (
            test_id SERIAL PRIMARY KEY,
            name VARCHAR(100),
            result VARCHAR(100),
            ordered_by VARCHAR(150)
        )
        """,
        """
        CREATE TABLE visit (
            visit_id SERIAL PRIMARY KEY,
            date VARCHAR(100),
            doctor_id int REFERENCES doctor (doctor_id),
            location_id int REFERENCES location (location_id),
            treatment VARCHAR(500)
        )
        """,
        """
        CREATE TABLE healthhistory (
            medical_id SERIAL PRIMARY KEY,
            pet_id int REFERENCES pet (pet_id),
            visit_id int REFERENCES visit (visit_id),
            medication_id int REFERENCES medication (medication_id),
            test_id int REFERENCES test (test_id)
        )
        """
        )

    conn = None
    try:
        conn = create_connection()      #establishes connection        
        c = conn.cursor()               #creates cursor to execute sql commands on

        for command in commands:        #iterates through commands to execute
            c.execute(command)
        print("All tables created")
        
        c.close()                       #closes cursor

        conn.commit()                   #commits command changes to database
    except (Exception, psycopg2.DatabaseError) as error:    #throws error if problem occurs in try block
        print(error)
    finally:                            #closes cursor connection if not connection is made
        if conn is not None:
            conn.close()


# ------------------------- Insert statement functions that create connection, execute the sql command with provided values, commits changes, and closes connection -------------------------

def insert_owners(owner_list):       #passes in list of values to be entered that matches the table attributes
    sql = """INSERT INTO owners(insurance_id, name, phone, address) VALUES(%s, %s, %s, %s) RETURNING owner_id"""
    GetID = "SELECT LASTVAL()"

    conn = None
    try:
        conn = create_connection()
        c = conn.cursor()
    
        c.executemany(sql, owner_list)  #executes the sql statement with the values passed through owner_list

        c.execute(GetID)                #executes selecting the last value entered which is the insert statement

        OwnerID = c.fetchone()[0]       #fetchone statement will return an array of values that was entered
                                        #and position [0] is the Primary ID of the row

        conn.commit()

        c.close()
        return OwnerID                  # returns the ID of the row so it can be used in subsequent entity insert statements
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()

# all insert functions below follow the same syntax as above. The only difference is the sql statement, parameter, and name of the ID variable

def insert_insurance(insurance_list):
    sql = "INSERT INTO insurance(policy_number, provider, phone) VALUES(%s, %s, %s) RETURNING insurance_id"

    GetID = "SELECT LASTVAL()"

    conn = None
    try:

        conn = create_connection()
        c = conn.cursor()

        c.executemany(sql, insurance_list)
        c.execute(GetID)
        InsuranceID = c.fetchone()[0]

        conn.commit()

        c.close()
        return InsuranceID
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()

def insert_pets(pet_list):
    sql = """INSERT INTO pet(owner_id, name, age, weight, species) VALUES(%s, %s, %s, %s, %s) RETURNING pet_id"""
    GetID = "SELECT LASTVAL()"

    conn = None
    try:
        conn = create_connection()
        c = conn.cursor()

        c.executemany(sql, pet_list)
        c.execute(GetID)
        PetID = c.fetchone()[0]

        conn.commit()

        c.close()
        return PetID
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()

def insert_doctor(doctor_list):
    sql = "INSERT INTO doctor(name, specialization, phone, address) VALUES(%s, %s, %s, %s) RETURNING doctor_id"
    GetID = "SELECT LASTVAL()"

    conn = None
    try:
        conn = create_connection()
        c = conn.cursor()

        c.executemany(sql, doctor_list)
        c.execute(GetID)
        DoctorID = c.fetchone()[0]

        conn.commit()

        c.close()
        return DoctorID
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()

def insert_location(location_list):
    sql = "INSERT INTO location(name, phone, address) VALUES(%s, %s, %s) RETURNING location_id"
    GetID = "SELECT LASTVAL()"

    conn = None
    try:
        conn = create_connection()
        c = conn.cursor()

        c.executemany(sql, location_list)
        c.execute(GetID)
        LocationID = c.fetchone()[0]

        conn.commit()

        c.close()
        return LocationID
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()

def insert_medication(medication_list):
    sql = "INSERT INTO medication(name, manufactorer, dose, prescribed_by) VALUES(%s, %s, %s, %s) RETURNING medication_id"
    GetID = "SELECT LASTVAL()"

    conn = None
    try:
        conn = create_connection()
        c = conn.cursor()

        c.executemany(sql, medication_list)
        c.execute(GetID)
        MedicationID = c.fetchone()[0]

        conn.commit()

        c.close()
        return MedicationID
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()

def insert_test(test_list):
    sql = "INSERT INTO test(name, result, ordered_by) VALUES(%s, %s, %s) RETURNING test_id"
    GetID = "SELECT LASTVAL()"

    conn = None
    try:
        conn = create_connection()
        c = conn.cursor()

        c.executemany(sql, test_list)
        c.execute(GetID)
        TestID = c.fetchone()[0]

        conn.commit()

        c.close()
        return TestID
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()

def insert_visit(visit_list):
    sql = "INSERT INTO visit(date, doctor_id, location_id, treatment) VALUES(%s, %s, %s, %s) RETURNING visit_id"
    GetID = "SELECT LASTVAL()"

    conn = None
    try:
        conn = create_connection()
        c = conn.cursor()

        c.executemany(sql, visit_list)
        c.execute(GetID)
        VisitID = c.fetchone()[0]

        conn.commit()

        c.close()
        return VisitID
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()

def insert_healthhistory(healthhistory_list):
    sql = "INSERT INTO healthhistory(pet_id, visit_id, medication_id, test_id) VALUES(%s, %s, %s, %s) RETURNING medical_id"
    GetID = "SELECT LASTVAL()"

    conn = None
    try:
        conn = create_connection()
        c = conn.cursor()

        c.executemany(sql, healthhistory_list)
        c.execute(GetID)
        MedicalID = c.fetchone()[0]

        conn.commit()

        c.close()
        return MedicalID
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


def delete():               #deletes tables, columns, etc. from the database
    conn = None             # dangerous function but for the purposes of testing was useful to use

    delete_tables = ("DROP TABLE doctor CASCADE",
                    "DROP TABLE healthhistory CASCADE",
                    "DROP TABLE insurance CASCADE",
                    "DROP TABLE location CASCADE",
                    "DROP TABLE medication CASCADE",
                    "DROP TABLE owners CASCADE",
                    "DROP TABLE pet CASCADE",
                    "DROP TABLE test CASCADE",
                    "DROP TABLE visit CASCADE",
    )

    try:
        conn = create_connection()      #creates connection
        c = conn.cursor()               #creates cursor

        for command in delete_tables:   #loops through delete statements dropping a table and all the tables needed to make it
            c.execute(command)
        print("All tables deleted")

        conn.commit()

        c.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()



# ------------------------- Functions that call insert statement and create the data to be inserted -------------------------


def create_insurance():                         #creates insurance entry
    fake = Faker()                              #creates a faker object that can be called to used functions of the library
    HaveInsurance = random.randint(0,1)         #randomly generates if the owner has insurance
    if (HaveInsurance == 1):
        InsuranceID = insert_insurance([        #calls insert function using the array of values passed through as insurance_list and assigns insuranceID to be the priamry key value passed through the insert statement
            (random.randint(1,1000), fake.company(), fake.phone_number())
            ])

    else:
        InsuranceID = None                      #if owner doesnt have insurance, sets the value to null
    return InsuranceID                          #returns insuranceID to be used in subsequent entity insert statements as a foreign key

# all create functions follow the same format as the create insurance function. Only difference is ID variable name, the values being passed to the insert statement, and the parameters (if the entity requires a foreign key it will be passed as a parameter)
def create_owners(insuranceID):
    fake = Faker()
    OwnerID = insert_owners([
            (insuranceID, fake.name(), fake.phone_number(), fake.address())
        ])
    return OwnerID

def create_pets(OwnerID):
    fake = Faker()
    PetID = insert_pets([
        (OwnerID, fake.name(), random.randint(0,100), 'n/a', 'n/a')
    ])
    return PetID

def create_location():
    fake = Faker()
    LocationID = insert_location([
        (fake.company(), fake.phone_number(), fake.address())
    ])
    return LocationID

def create_doctor():
    fake = Faker()
    DoctorID = insert_doctor([
        (fake.name(), fake.name(), fake.phone_number(), fake.address())
    ])
    return DoctorID

def create_visit(DoctorID, LocationID):
    fake = Faker()
    VisitID = insert_visit([
        (fake.date(), DoctorID, LocationID, fake.text())
    ])
    return VisitID

def create_test():
    fake = Faker()
    TestID = insert_test([
        (fake.name(), fake.name(), fake.name())
    ])
    return TestID

def create_medication():
    fake = Faker()
    MedicationID = insert_medication([
        (fake.name(), fake.company(), random.randint(1, 200), fake.name())
    ])
    return MedicationID

def create_healthhistory(PetID, VisitID, MedicationID, TestID):
    fake = Faker()
    healthhistoryID = insert_healthhistory([
        (PetID, VisitID, MedicationID, TestID)
    ])
    return healthhistoryID



# ------------------------- Functions that call insert statement and create the data to be inserted -------------------------

def create_baseline(num):           # creates num baseline entrys with a 1 to 1 relation (one owner, one pet, one doctor, one visit etc.)

    fake = Faker()

    create_tables()

    RecordsIncrement = 0

    while RecordsIncrement < num:   # while to record increment is below the number of records wanted

        #Owner creation
        InsuranceID = create_insurance()
        OwnerID = create_owners(InsuranceID)

        #pet creation
        PetID = create_pets(OwnerID)

        DoctorID = create_doctor()
                
        #creates location for visit
        LocationID = create_location()
                
        #creates visitation for medical records
        VisitID = create_visit(DoctorID, LocationID)

        #generates meciation for a visit, can be null
        MedicationID = None
        numOfMeds = random.randint(0,1)
        if (numOfMeds == 0):
            MedicationID = create_medication()

        #generates test for a visit, can be null
        TestID = None
        numOfTests = random.randint(0,1)
        if (numOfTests == 0):
            TestID = create_test()

        # generates healthhistory entry with IDs of previous entities
        MedID = create_healthhistory(PetID, VisitID, MedicationID, TestID)

        RecordsIncrement += 1

    #prints the number of records entered to ensure correct amount was made
    print("Records = ", RecordsIncrement)       



# creates record entries that would mirror an actual entry
def create_records(num):
    fake = Faker()

    create_tables()         #creates all tables

    RecordsIncrement = 0    #used to increment through num of records wanted

    while RecordsIncrement < num:           #Creates the number of records wanted

        # randomly generates if the owner will have insurance, returns ID to insert into owners
        InsuranceID = create_insurance()

        #generates the amount of owners, returns ID for creating pets
        OwnerID = create_owners(InsuranceID)

        #generates pets that are linked to one owner, ASSUMPTION: a owner has 1 <= pets <= 3
        numOfPets = random.randint(1, 3)
        petIncrement = 0
        while (petIncrement < numOfPets):

            #creates PetID to be used in creating medical records
            PetID = create_pets(OwnerID)
            petIncrement += 1

            #generates the amount of medical records for each pet ASSUMPTION: A pet has no more than 5 records
            numOfMedicalRecords = random.randint(1,5) 
            medicalIncrement = 0
            while (medicalIncrement < numOfMedicalRecords):
                
                #creates doctor for visit
                DoctorID = create_doctor()
                
                #creates location for visit
                LocationID = create_location()
                
                #creates visitation for medical records
                VisitID = create_visit(DoctorID, LocationID)

                #generates medication from a visit, can be null  ASSUMPTION: at most one medication and/or one test is done at visit
                MedicationID = None
                TestID = None
                numOfMeds = random.randint(0,1)
                if (numOfMeds == 0):
                    MedicationID = create_medication()

                #generates test for a visit, can be null
                numOfTests = random.randint(0,1)
                if (numOfTests == 0):
                    TestID = create_test()

                # generates pets medical_history
                MedID = create_healthhistory(PetID, VisitID, MedicationID, TestID)

                medicalIncrement += 1

        print("entered record = %d" % (RecordsIncrement+1)) #prints what number record was entered. Useful to see progress on large number of entries
        RecordsIncrement += 1

    #prints the number of records entered to ensure correct amount was made
    print("Records = ", RecordsIncrement) 


if __name__ == '__main__':
    start_time = time.time()    #used to time the execution time of script

    #create_baseline(10)
    #create_records(10)
    #delete()                   #Uncomment if you want to delete all tables and re-enter fields

    print("----- %s seconds -----" % (time.time() - start_time)) #prints time taken for execution









