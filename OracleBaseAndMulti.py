#!/usr/bin/env python

import cx_Oracle                    # database work in python uses cx_Oracle library to connect to oracle database
from cx_Oracle import Error
import pandas.io.sql as psql
import random
import string
from faker import Faker
import time


def create_connection():            #establishes connection with student database on AWS
    conn = None
    try:
        dsn_tns = cx_Oracle.makedsn('student.canlgkvkulnk.us-west-2.rds.amazonaws.com', '1521', service_name='ORCL')
        conn = cx_Oracle.connect(user=r'admin', password='4KkghduiQui8Gb7', dsn=dsn_tns)

        return conn
    except Error as e:          #if no connection throw error

        print(e)
    
    return conn                 # returns connection to be used for command executions

def create_tables():         

    commands = (                #creates the SQL commands to be used in creating the tables
        """
        CREATE TABLE insurance (
            insurance_id NUMBER GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1),
            policy_number int,
            provider VARCHAR2(100),
            phone VARCHAR2(100),
            PRIMARY KEY(insurance_id)
        )
        """,
        """
        CREATE TABLE owner (
            owner_id NUMBER GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1),
            insurance_id NUMBER,
            name VARCHAR2(100) NOT NULL,
            phone VARCHAR2(50),
            address VARCHAR2(100),
            PRIMARY KEY(owner_id),
            FOREIGN KEY (insurance_id) REFERENCES insurance (insurance_id)

        )
        """,
        """ 
        CREATE TABLE pet (
            pet_id NUMBER GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1),
            owner_id NUMBER,
            name VARCHAR2(100),
            age NUMBER,
            weight VARCHAR2(20),
            species VARCHAR2(100),
            PRIMARY KEY(pet_id),
            FOREIGN KEY(owner_id) REFERENCES owner (owner_id)
        )
        """,
        """
        CREATE TABLE doctor (
            doctor_id NUMBER GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1),
            name VARCHAR2(100),
            specialization VARCHAR(100),
            phone VARCHAR2(100),
            address VARCHAR2(150),
            PRIMARY KEY(doctor_id)
        )
        """,
        """
        CREATE TABLE location (
            location_id NUMBER GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1),
            name VARCHAR2(100),
            phone VARCHAR2(100),
            address VARCHAR2(150),
            PRIMARY KEY(location_id)
        )
        """,
        """
        CREATE TABLE medication (
            medication_id NUMBER GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1),
            name VARCHAR2(100),
            manufactorer VARCHAR2(100),
            dose VARCHAR(100),
            prescribed_by VARCHAR2(150),
            PRIMARY KEY(medication_id)
        )
        """,
        """
        CREATE TABLE test (
            test_id NUMBER GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1),
            name VARCHAR2(100),
            result VARCHAR2(100),
            ordered_by VARCHAR2(150),
            PRIMARY KEY(test_id)
        )
        """,
        """
        CREATE TABLE visit (
            visit_id NUMBER GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1),
            datee VARCHAR2(100),
            doctor_id NUMBER,
            location_id NUMBER,
            treatment VARCHAR2(500),
            PRIMARY KEY(visit_id),
            FOREIGN KEY (doctor_id) REFERENCES doctor(doctor_id),
            FOREIGN KEY (location_id) REFERENCES location(location_id)
        )
        """,
        """
        CREATE TABLE healthhistory (
            healthhistory_id NUMBER GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1),
            pet_id NUMBER,
            visit_id NUMBER,
            medication_id NUMBER,
            test_id NUMBER,
            PRIMARY KEY(healthhistory_id),
            FOREIGN KEY(pet_id) REFERENCES pet(pet_id),
            FOREIGN KEY(visit_id) REFERENCES visit(visit_id),
            FOREIGN KEY(medication_id) REFERENCES medication(medication_id),
            FOREIGN KEY(test_id) REFERENCES test(test_id)
        )
        """
        )

    conn = None
    try:
        conn = create_connection()      #establishes connection        
        c = conn.cursor()               #creates cursor to execute sql commands on

        for command in commands:        #iterates through commands to execute
            c.execute(command)
        print("All tables created")
        
        c.close()                       #closes cursor

        conn.commit()                   #commits command changes to database
    except (Exception, Error) as error:    #throws error if problem occurs in try block
        print("Tables not created")
        print("error = ", error)
    finally:                            #closes cursor connection if not connection is made
        if conn is not None:
            conn.close()


# ------------------------- Insert statement functions that create connection, execute the sql command with provided values, commits changes, and closes connection -------------------------


def insert_insurance(insurance_list):

    sql = ('insert into insurance(policy_number, provider, phone)' 'values(:policy_number, :provider, :phone)') # sql statement that alows insertion

    sql_select = ('SELECT insurance_id FROM insurance WHERE insurance_id = (SELECT MAX(insurance_id) FROM insurance)') # sql statement that returns the primary key of the entry just inserted

    try:
        connection = create_connection() # creates connection
        cursor = connection.cursor()    # creates cursor from connection where sql statements will be executed

        #sql_params = {'newest_id_sql_param' : newest_id }


        cursor.execute(sql, [insurance_list[0][0], insurance_list[0][1], insurance_list[0][2]]) #executes the sql statement with the values inputed through insurance_list
        #cursor.execute(sql_params)

        cursor.execute(sql_select)  # gets last primary key of entyr just inserted
        insuranceID = cursor.fetchone()[0]  #assigns primary key to variable

        connection.commit() #commits changes

        cursor.close()  #closes the cursor
        connection.close()  #closes the connection

        return insuranceID  #returns Primary key ID of what was just entered
    except cx_Oracle.Error as error:
        print("error in inserting insurance")
        print(error)


#all function that follow have the same structure the insert_insurance functon.
#The only difference is variable names and the type of data being passed into the function
    
def insert_owner(owner_list):

    sql = ('insert into owner(insurance_id, name, phone, address)' 'values(:insurance_id, :name, :phone, :address)')

    sql_select = ('SELECT owner_id FROM owner WHERE owner_id = (SELECT MAX(owner_id) FROM owner)')

    try:
        connection = create_connection()
        cursor = connection.cursor()

        #sql_params = {'newest_id_sql_param' : newest_id }


        cursor.execute(sql, [owner_list[0][0], owner_list[0][1], owner_list[0][2], owner_list[0][3]])
        #cursor.execute(sql_params)

        cursor.execute(sql_select)
        ownerID = cursor.fetchone()[0]

        connection.commit()

        cursor.close()
        connection.close()

        return ownerID
    except cx_Oracle.Error as error:
        print("error in inserting insurance")
        print(error)



def insert_pet(pet_list):

    sql = ('insert into pet(owner_id, name, age, weight, species)' 'values(:owner_id, :name, :age, :weight, :species)')

    sql_select = ('SELECT pet_id FROM pet WHERE pet_id = (SELECT MAX(pet_id) FROM pet)')

    try:
        connection = create_connection()
        cursor = connection.cursor()

        #sql_params = {'newest_id_sql_param' : newest_id }


        cursor.execute(sql, [pet_list[0][0], pet_list[0][1], pet_list[0][2], pet_list[0][3], pet_list[0][4]])
        #cursor.execute(sql_params)

        cursor.execute(sql_select)
        petID = cursor.fetchone()[0]

        connection.commit()

        cursor.close()
        connection.close()

        return petID

    except cx_Oracle.Error as error:
        print("error in inserting pet")
        print(error)

def insert_location(location_list):

    sql = ('insert into location(name, phone, address)' 'values(:name, :phone, :address)')

    sql_select = ('SELECT location_id FROM location WHERE location_id = (SELECT MAX(location_id) FROM location)')

    try:
        connection = create_connection()
        cursor = connection.cursor()

        #sql_params = {'newest_id_sql_param' : newest_id }


        cursor.execute(sql, [location_list[0][0], location_list[0][1], location_list[0][2]])
        #cursor.execute(sql_params)

        cursor.execute(sql_select)
        locationID = cursor.fetchone()[0]

        connection.commit()

        cursor.close()
        connection.close()

        return locationID
    except cx_Oracle.Error as error:
        print("error in inserting location")
        print(error)



def insert_medication(medication_list):

    sql = ('insert into medication(name, manufactorer, dose, prescribed_by)' 'values(:name, :manufactorer, :dose, :prescribed_by)')

    sql_select = ('SELECT medication_id FROM medication WHERE medication_id = (SELECT MAX(medication_id) FROM medication)')

    try:
        connection = create_connection()
        cursor = connection.cursor()

        #sql_params = {'newest_id_sql_param' : newest_id }


        cursor.execute(sql, [medication_list[0][0], medication_list[0][1], medication_list[0][2], medication_list[0][3]])
        #cursor.execute(sql_params)

        cursor.execute(sql_select)
        medicationID = cursor.fetchone()[0]

        connection.commit()

        cursor.close()
        connection.close()

        return medicationID
    except cx_Oracle.Error as error:
        print("error in inserting medication")
        print(error)
    

def insert_doctor(doctor_list):

    sql = ('insert into doctor(name, specialization, phone, address)' 'values(:name, :specialization, :phone, :address)')

    sql_select = ('SELECT doctor_id FROM doctor WHERE doctor_id = (SELECT MAX(doctor_id) FROM doctor)')

    try:
        connection = create_connection()
        cursor = connection.cursor()

        #sql_params = {'newest_id_sql_param' : newest_id }


        cursor.execute(sql, [doctor_list[0][0], doctor_list[0][1], doctor_list[0][2], doctor_list[0][3]])
        #cursor.execute(sql_params)

        cursor.execute(sql_select)
        doctorID = cursor.fetchone()[0]

        connection.commit()

        cursor.close()
        connection.close()

        return doctorID
    except cx_Oracle.Error as error:
        print("error in inserting doctor")
        print(error)


def insert_test(test_list):

    sql = ('insert into test(name, result, ordered_by)' 'values(:name, :result, :ordered_by)')

    sql_select = ('SELECT test_id FROM test WHERE test_id = (SELECT MAX(test_id) FROM test)')

    try:
        connection = create_connection()
        cursor = connection.cursor()

        #sql_params = {'newest_id_sql_param' : newest_id }


        cursor.execute(sql, [test_list[0][0], test_list[0][1], test_list[0][2]])
        #cursor.execute(sql_params)

        cursor.execute(sql_select)
        testID = cursor.fetchone()[0]

        connection.commit()

        cursor.close()
        connection.close()

        return testID
    except cx_Oracle.Error as error:
        print("error in inserting test")
        print(error)


def insert_visit(visit_list):

    sql = ('insert into visit(datee, doctor_id, location_id, treatment)' 'values(:datee, :doctor_id, :location_id, :treatment)')

    sql_select = ('SELECT visit_id FROM visit WHERE visit_id = (SELECT MAX(visit_id) FROM visit)')

    try:
        connection = create_connection()
        cursor = connection.cursor()

        #sql_params = {'newest_id_sql_param' : newest_id }


        cursor.execute(sql, [visit_list[0][0], visit_list[0][1], visit_list[0][2],visit_list[0][3]])
        #cursor.execute(sql_params)

        cursor.execute(sql_select)
        visitID = cursor.fetchone()[0]

        connection.commit()

        cursor.close()
        connection.close()

        return visitID
    except cx_Oracle.Error as error:
        print("error in inserting visit")
        print(error)


def insert_healthhistory(healthhistory_list):

    sql = ('insert into healthhistory(pet_id, visit_id, medication_id, test_id)' 'values(:pet_id, :visit_id, :medication_id, :test_id)')

    sql_select = ('SELECT healthhistory_id FROM healthhistory WHERE healthhistory_id = (SELECT MAX(healthhistory_id) FROM healthhistory)')

    try:
        connection = create_connection()
        cursor = connection.cursor()

        #sql_params = {'newest_id_sql_param' : newest_id }


        cursor.execute(sql, [healthhistory_list[0][0], healthhistory_list[0][1], healthhistory_list[0][2], healthhistory_list[0][3]])
        #cursor.execute(sql_params)

        cursor.execute(sql_select)
        healthhistoryID = cursor.fetchone()[0]

        connection.commit()

        cursor.close()
        connection.close()

        return healthhistoryID
    except cx_Oracle.Error as error:
        print("error in inserting healthhistory")
        print(error)


#  ------------------------ Delete tables function ------------------------

def delete():               #deletes tables, columns, etc. from the database
    conn = None             # dangerous function but for the purposes of testing was useful to use

    delete_tables = ("DROP TABLE doctor CASCADE CONSTRAINTS",
                    "DROP TABLE healthhistory CASCADE CONSTRAINTS",
                    "DROP TABLE insurance CASCADE CONSTRAINTS",
                    "DROP TABLE location CASCADE CONSTRAINTS",
                    "DROP TABLE medication CASCADE CONSTRAINTS",
                    "DROP TABLE owner CASCADE CONSTRAINTS",
                    "DROP TABLE pet CASCADE CONSTRAINTS",
                    "DROP TABLE test CASCADE CONSTRAINTS",
                    "DROP TABLE visit CASCADE CONSTRAINTS",
    )

    try:
        conn = create_connection()      #creates connection
        c = conn.cursor()               #creates cursor

        for command in delete_tables:   #loops through delete statements dropping a table and all the tables needed to make it
            c.execute(command)
        print("All tables deleted")

        conn.commit()

        c.close()
    except (Exception, cx_Oracle.DatabaseError) as error:
        print("All not tables deleted")
        print(error)
    finally:
        if conn is not None:
            conn.close()



# ------------------------- Functions that call insert statement and create the data to be inserted -------------------------


def create_insurance():
    fake = Faker()

    number = random.randint(1,1000) #generates a random policy number for insertion

    insuranceId = insert_insurance([        #calls insert function using the array of values passed through as insurance_list and assigns insuranceID to be the priamry key value passed through the insert statement
            (number, fake.company(), fake.phone_number())
            ])

    return insuranceId

# function below follows the same format as create_insurance just with different inputs lists and generated data

def create_owner(InsuranceId):
    fake = Faker()

    ownerId = insert_owner([       
            (InsuranceId, fake.name(), fake.phone_number(), fake.address())
            ])

    return ownerId

def create_pet(OwnerId):
    fake = Faker()
    PetId = insert_pet([        
            (OwnerId, fake.name(), random.randint(0,100), 'n/a', 'n/a')
            ])

    return PetId

def create_location():
    fake = Faker()
    LocationID = insert_location([
        (fake.company(), fake.phone_number(), fake.address())
    ])
    return LocationID

def create_medication():
    fake = Faker()
    medicationID = insert_medication([
        ('name', fake.company(), '--mg', fake.name())
    ])
    return medicationID

def create_doctor():
    fake = Faker()
    doctorID = insert_doctor([
        (fake.name(), 'specialization', fake.phone_number(), fake.address())
    ])
    return doctorID

def create_test():
    fake = Faker()
    testID = insert_test([
        ('test name', 'result', fake.name())
    ])
    return testID

def create_visit(DoctorId, LocationId):
    fake = Faker()
    visitID = insert_visit([
        ('date', DoctorId, LocationId, 'treatment')
    ])
    return visitID

def create_healthhistory(PetID, VisitID, MedicationID, TestID):
    fake = Faker()
    healthhistoryID = insert_healthhistory([
        (PetID, VisitID, MedicationID, TestID)
    ])
    return healthhistoryID



# ------------------------- Functions that call insert statement and create the data to be inserted -------------------------

def create_record():

    create_tables()

    insuranceID = create_insurance()
    ownerId = create_owner(insuranceID)
    PetId = create_pet(ownerId)
    LocationId = create_location()
    MedicationId = create_medication()
    DoctorId = create_doctor()
    TestId = create_test()
    VisitId = create_visit(DoctorId, LocationId)


def create_baseline(num):           # creates num baseline entrys with a 1 to 1 relation (one owner, one pet, one doctor, one visit etc.)

    fake = Faker()

    create_tables()

    RecordsIncrement = 0

    while RecordsIncrement < num:   # while to record increment is below the number of records wanted

        #Owner creation
        InsuranceID = create_insurance()
        OwnerID = create_owner(InsuranceID)

        #pet creation
        PetID = create_pet(OwnerID)

        DoctorID = create_doctor()
                
        #creates location for visit
        LocationID = create_location()
                
        #creates visitation for medical records
        VisitID = create_visit(DoctorID, LocationID)

        #generates meciation for a visit, can be null
        MedicationID = None
        numOfMeds = random.randint(0,1)
        if (numOfMeds == 0):
            MedicationID = create_medication()

        #generates test for a visit, can be null
        TestID = None
        numOfTests = random.randint(0,1)
        if (numOfTests == 0):
            TestID = create_test()

        # generates healthhistory entry with IDs of previous entities
        MedID = create_healthhistory(PetID, VisitID, MedicationID, TestID)

        RecordsIncrement += 1

    #prints the number of records entered to ensure correct amount was made
    print("Records = ", RecordsIncrement)


def create_records(num):
    fake = Faker()

    create_tables()         #creates all tables

    RecordsIncrement = 0    #used to increment through num of records wanted

    while RecordsIncrement < num:           #Creates the number of records wanted

        # randomly generates if the owner will have insurance, returns ID to insert into owners
        InsuranceID = create_insurance()

        #generates the amount of owners, returns ID for creating pets
        OwnerID = create_owner(InsuranceID)

        #generates pets that are linked to one owner, ASSUMPTION: a owner has 1 <= pets <= 3
        numOfPets = random.randint(1, 3)
        petIncrement = 0
        while (petIncrement < numOfPets):

            #creates PetID to be used in creating medical records
            PetID = create_pet(OwnerID)
            petIncrement += 1

            #generates the amount of medical records for each pet ASSUMPTION: A pet has no more than 5 records
            numOfMedicalRecords = random.randint(1,5) 
            medicalIncrement = 0
            while (medicalIncrement < numOfMedicalRecords):
                
                #creates doctor for visit
                DoctorID = create_doctor()
                
                #creates location for visit
                LocationID = create_location()
                
                #creates visitation for medical records
                VisitID = create_visit(DoctorID, LocationID)

                #generates medication from a visit, can be null  ASSUMPTION: at most one medication and/or one test is done at visit
                MedicationID = None
                TestID = None
                numOfMeds = random.randint(0,1)
                if (numOfMeds == 0):
                    MedicationID = create_medication()

                #generates test for a visit, can be null
                numOfTests = random.randint(0,1)
                if (numOfTests == 0):
                    TestID = create_test()

                # generates pets medical_history
                MedID = create_healthhistory(PetID, VisitID, MedicationID, TestID)

                medicalIncrement += 1

        print("entered record = %d" % (RecordsIncrement+1)) #prints what number record was entered. Useful to see progress on large number of entries
        RecordsIncrement += 1

    #prints the number of records entered to ensure correct amount was made
    print("Records = ", RecordsIncrement) 


if __name__ == '__main__':
    start_time = time.time()    #used to time the execution time of script
    fake = Faker()

    #create_baseline(1000)
    create_records(1000)


    #delete()                   #Uncomment if you want to delete all tables and re-enter fields

    print("----- %s seconds -----" % (time.time() - start_time)) #prints time taken for execution



