#!/usr/bin/env python

import psycopg2
from psycopg2 import Error
import pandas.io.sql as psql
import random
import string
from faker import Faker
import time


def create_connection():            #establishes connection with "healthtest" database
    conn = None
    try:
        conn = psycopg2.connect(    #parameters to create connection
            user = "Brandon",
            host = "localhost",
            port = "5432",
            database = "healthtest")
        return conn
    except Error as e:              #if no connection throw error
        print(e)
    
    return conn                     # returns connection to be used for command executions

def create_tables():            #creates tables are database tables

    commands = (                #creates the SQL commands to be used
        """
        CREATE TABLE insurance (
            insurance_id SERIAL PRIMARY KEY,
            policy_number int,
            provider VARCHAR(100),
            phone VARCHAR(100)
        )
        """,
        """
        CREATE TABLE owners (
            owner_id SERIAL PRIMARY KEY,
            insurance_id int REFERENCES insurance (insurance_id),
            name VARCHAR(100) NOT NULL,
            phone VARCHAR(50),
            address VARCHAR(100)
        )
        """,
        """ 
        CREATE TABLE pet (
            pet_id SERIAL PRIMARY KEY,
            owner_id int REFERENCES owners (owner_id) NOT NULL,
            name VARCHAR(100),
            age int,
            weight VARCHAR(20),
            species VARCHAR(100)
        )
        """,
        """
        CREATE TABLE doctor (
            doctor_id SERIAL PRIMARY KEY,
            name VARCHAR(100),
            specialization VARCHAR(100),
            phone VARCHAR(100),
            address VARCHAR(150)
        )
        """,
        """
        CREATE TABLE location (
            location_id SERIAL PRIMARY KEY,
            name VARCHAR(100),
            phone VARCHAR(100),
            address VARCHAR(150)
        )
        """,
        """
        CREATE TABLE medication (
            medication_id SERIAL PRIMARY KEY,
            name VARCHAR(100),
            manufactorer VARCHAR(100),
            dose VARCHAR(100),
            prescribed_by VARCHAR(150)
        )
        """,
        """
        CREATE TABLE test (
            test_id SERIAL PRIMARY KEY,
            name VARCHAR(100),
            result VARCHAR(100),
            ordered_by VARCHAR(150)
        )
        """,
        """
        CREATE TABLE visit (
            visit_id SERIAL PRIMARY KEY,
            date VARCHAR(100),
            doctor_id int REFERENCES doctor (doctor_id),
            location_id int REFERENCES location (location_id),
            treatment VARCHAR(500)
        )
        """,
        """
        CREATE TABLE healthhistory (
            medical_id SERIAL PRIMARY KEY,
            pet_id int REFERENCES pet (pet_id),
            visit_id int REFERENCES visit (visit_id),
            medication_id int REFERENCES medication (medication_id),
            test_id int REFERENCES test (test_id)
        )
        """
        )

    conn = None
    try:
        conn = create_connection()      #establishes connection        
        c = conn.cursor()               #creates cursor

        for command in commands:        #iterates through commands to execute
            c.execute(command)
        print("All tables created")
        
        c.close()                       #closes cursor

        conn.commit()                   #commits command changes to database
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()

def insert_owners():
    sql = """INSERT INTO owners(insurance_id, name, phone, address) VALUES(%s, %s, %s, %s) RETURNING owner_id"""
    GetID = "SELECT LASTVAL()"

    conn = None
    try:
        conn = create_connection()
        c = conn.cursor()
        f = open(r'/Users/Brandon/PythonWork/owner.csv', 'r')
        c.copy_from(f, 'owners', sep='|')
        f.close()

        conn.commit()

        c.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()

def insert_insurance():
    sql = "INSERT INTO insurance(policy_number, provider, phone) VALUES(%s, %s, %s) RETURNING insurance_id"

    GetID = "SELECT LASTVAL()"

    conn = None
    try:

        conn = create_connection()
        c = conn.cursor()
        f = open(r'/Users/Brandon/PythonWork/insurance.csv', 'r')
        c.copy_from(f, 'insurance', sep='|')
        f.close()

        conn.commit()

        c.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()

def insert_pets():
    sql = """INSERT INTO pet(owner_id, name, age, weight, species) VALUES(%s, %s, %s, %s, %s) RETURNING pet_id"""
    GetID = "SELECT LASTVAL()"

    conn = None
    try:
        conn = create_connection()
        c = conn.cursor()

        f = open(r'/Users/Brandon/PythonWork/pet.csv', 'r')
        c.copy_from(f, 'pet', sep='|')
        f.close()


        conn.commit()

        c.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()

def insert_doctor():
    sql = "INSERT INTO doctor(name, specialization, phone, address) VALUES(%s, %s, %s, %s) RETURNING doctor_id"
    GetID = "SELECT LASTVAL()"

    conn = None
    try:
        conn = create_connection()
        c = conn.cursor()

        f = open(r'/Users/Brandon/PythonWork/doctor.csv', 'r')
        c.copy_from(f, 'doctor', sep='|')
        f.close()

        conn.commit()

        c.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()

def insert_location():
    sql = "INSERT INTO location(name, phone, address) VALUES(%s, %s, %s) RETURNING location_id"
    GetID = "SELECT LASTVAL()"

    conn = None
    try:
        conn = create_connection()
        c = conn.cursor()

        f = open(r'/Users/Brandon/PythonWork/location.csv', 'r')
        c.copy_from(f, 'location', sep='|')
        f.close()

        conn.commit()

        c.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()

def insert_medication():
    sql = "INSERT INTO medication(name, manufactorer, dose, prescribed_by) VALUES(%s, %s, %s, %s) RETURNING medication_id"
    GetID = "SELECT LASTVAL()"

    conn = None
    try:
        conn = create_connection()
        c = conn.cursor()

        f = open(r'/Users/Brandon/PythonWork/medication.csv', 'r')
        c.copy_from(f, 'medication', sep='|')
        f.close()

        conn.commit()

        c.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()

def insert_test():
    sql = "INSERT INTO test(name, result, ordered_by) VALUES(%s, %s, %s) RETURNING test_id"
    GetID = "SELECT LASTVAL()"

    conn = None
    try:
        conn = create_connection()
        c = conn.cursor()

        f = open(r'/Users/Brandon/PythonWork/test.csv', 'r')
        c.copy_from(f, 'test', sep='|')
        f.close()

        #c.executemany(sql, test_list)
        #c.execute(GetID)
        #TestID = c.fetchone()[0]

        conn.commit()

        c.close()
        #return TestID
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()

def insert_visit():
    sql = "INSERT INTO visit(date, doctor_id, location_id, treatment) VALUES(%s, %s, %s, %s) RETURNING visit_id"
    GetID = "SELECT LASTVAL()"

    conn = None
    try:
        conn = create_connection()
        c = conn.cursor()

        f = open(r'/Users/Brandon/PythonWork/visit.csv', 'r')
        c.copy_from(f, 'visit', sep='|')
        f.close()

        #c.executemany(sql, visit_list)
        #c.execute(GetID)
        #VisitID = c.fetchone()[0]

        conn.commit()

        c.close()
        #return VisitID
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()

def insert_healthhistory():
    sql = "INSERT INTO healthhistory(pet_id, visit_id, medication_id, test_id) VALUES(%s, %s, %s, %s) RETURNING medical_id"
    GetID = "SELECT LASTVAL()"

    conn = None
    try:
        conn = create_connection()
        c = conn.cursor()

        f = open(r'/Users/Brandon/PythonWork/healthhistory.csv', 'r')
        c.copy_from(f, 'healthhistory', sep='|')
        f.close()

        #c.executemany(sql, healthhistory_list)
        #c.execute(GetID)
        #MedicalID = c.fetchone()[0]

        conn.commit()

        c.close()
        #return MedicalID
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


def delete():               #deletes tables, columns, etc. from the database
    conn = None

    delete_tables = ("DROP TABLE doctor CASCADE",
                    "DROP TABLE healthhistory CASCADE",
                    "DROP TABLE insurance CASCADE",
                    "DROP TABLE location CASCADE",
                    "DROP TABLE medication CASCADE",
                    "DROP TABLE owners CASCADE",
                    "DROP TABLE pet CASCADE",
                    "DROP TABLE test CASCADE",
                    "DROP TABLE visit CASCADE",
    )

    try:
        conn = create_connection()      #creates connection
        c = conn.cursor()               #creates cursor

        for command in delete_tables:
            c.execute(command)
        print("All tables deleted")

        conn.commit()

        c.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


def create_csvRecords():
    create_tables()
    insert_insurance()
    insert_owners()
    insert_pets()
    insert_doctor()
    insert_test()
    insert_medication()
    insert_location()
    insert_visit()
    insert_healthhistory()




if __name__ == '__main__':
    start_time = time.time()

    #create_csvRecords()         #Create CSV to feed in and enters them

    delete()                   #Uncomment if you want to delete all tables and re-enter fields

    print("----- %s seconds -----" % (time.time() - start_time))









