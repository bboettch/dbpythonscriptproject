import csv
from faker import Faker
import time
import random

if __name__ == '__main__':      #This file generates the multiple csvs that will be inserted into the database.
                                #There are 9 csv generation statements, one for each table of the database

    start_time = time.time()    #used to time exectuion of the script

    fake = Faker()              #creates faker object that can be used to generate random data
    
    recordNum = 1000            #number of records that would like to be inserted into the database

    insuranceIncrement = 0      #increments of all the tables to ensure each csv row amount equals the number of records to be inserted
    ownerIncrement = 0
    petIncrement = 0
    healthhistoryIncrement = 0
    locationIncrement = 0
    medicationIncrement = 0
    testIncrement = 0
    doctorIncrement = 0
    visitIncrement = 0


    # Creates insurance csv
    #open the csv file with the ability to write naming it insurance_file
    with open('insurance.csv', mode='w') as insurance_file:

        # write to the file using | as a delimiter character and quoting minimal entries
        insurance_writer = csv.writer(insurance_file, delimiter='|', quoting = csv.QUOTE_MINIMAL)

        while (insuranceIncrement < recordNum):
            # call the writer object from csv library to write to the file a new row that accepts the values for an insert of one entry
            insurance_writer.writerow([insuranceIncrement, random.randint(1, recordNum-1), fake.company(), fake.phone_number()])
            insuranceIncrement += 1

    # all subsequent functions follow the same format as the above. Only differences include file names, increment names, and writerow() parameters

    # Creates owner csv
    with open('owner.csv', mode='w') as owner_file:
        owner_writer = csv.writer(owner_file, delimiter='|', quoting = csv.QUOTE_MINIMAL)

        while (ownerIncrement < recordNum):
            owner_writer.writerow([ownerIncrement, random.randint(0, recordNum -1), fake.name(), fake.phone_number(), 'address'])
            ownerIncrement += 1
    
    # Creates pet csv
    with open('pet.csv', mode='w') as pet_file:
        pet_writer = csv.writer(pet_file, delimiter='|', quoting = csv.QUOTE_MINIMAL)

        while (petIncrement < recordNum):
            pet_writer.writerow([petIncrement, random.randint(0,recordNum - 1), fake.name(), random.randint(1, 50), "wieght", "species"])
            petIncrement += 1

    with open('healthhistory.csv', mode='w') as healthhistory_file:
        healthhistory_writer = csv.writer(healthhistory_file, delimiter='|', quoting = csv.QUOTE_MINIMAL)

        while (healthhistoryIncrement < recordNum):
            healthhistory_writer.writerow([healthhistoryIncrement, random.randint(0, recordNum - 1), random.randint(0, recordNum - 1), random.randint(0, recordNum - 1), random.randint(0, recordNum - 1)])
            healthhistoryIncrement += 1

    with open('location.csv', mode='w') as location_file:
        location_writer = csv.writer(location_file, delimiter='|', quoting = csv.QUOTE_MINIMAL)

        while (locationIncrement < recordNum):
            location_writer.writerow([locationIncrement, fake.name(), fake.phone_number(), 'address'])
            locationIncrement += 1


    with open('doctor.csv', mode='w') as doctor_file:
        doctor_writer = csv.writer(doctor_file, delimiter='|', quoting = csv.QUOTE_MINIMAL)

        while (doctorIncrement < recordNum):
            doctor_writer.writerow([doctorIncrement, fake.name(), "doctor", fake.phone_number(), 'address'])
            doctorIncrement += 1

    with open('medication.csv', mode='w') as medication_file:
        medication_writer = csv.writer(medication_file, delimiter='|', quoting = csv.QUOTE_MINIMAL)

        while (medicationIncrement < recordNum):
            medication_writer.writerow([medicationIncrement, fake.name(), fake.company(), 'dose', fake.name()])
            medicationIncrement += 1

    with open('test.csv', mode='w') as test_file:
        test_writer = csv.writer(test_file, delimiter='|', quoting = csv.QUOTE_MINIMAL)

        while (testIncrement < recordNum):
            test_writer.writerow([testIncrement, "testname", "result", fake.name()])
            testIncrement += 1

    with open('visit.csv', mode='w') as visit_file:
        visit_writer = csv.writer(visit_file, delimiter='|', quoting = csv.QUOTE_MINIMAL)

        while (visitIncrement < recordNum):
            visit_writer.writerow([visitIncrement, "date", random.randint(0,recordNum-1), random.randint(0,recordNum-1), "treatment"])
            visitIncrement += 1

    print("All csvs created")

    print("--------- %s seconds ---------" % (time.time() - start_time))


    


    
    