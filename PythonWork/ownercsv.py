import csv
from faker import Faker
import time
import random

if __name__ == '__main__':

    start_time = time.time()

    fake = Faker()

    num = 10
    i = 0
    j = 0

    with open('insurance.csv', mode='w') as insurance_file:
        insurance_writer = csv.writer(insurance_file, delimiter='|', quoting = csv.QUOTE_MINIMAL)

        while (i < num):
            insurance_writer.writerow([i, random.randint(1, 1000), fake.company(), fake.phone_number()])
            i += 1

    with open('owner.csv', mode='w') as owner_file:
        owner_writer = csv.writer(owner_file, delimiter='|', quoting = csv.QUOTE_MINIMAL)

        while (j < num):
            owner_writer.writerow([j, 0, fake.name(), fake.phone_number(), 'address'])
            j += 1
    

    with open('owner.csv', mode='rt') as f:
        csv_reader = csv.reader(f)

        for line in csv_reader:
            print(line[0])
            print(line[1])
            print(line[2])
            print(line[3])
            print(line[4])
            print("---------------------------------")

    print("----- %s seconds -----" % (time.time() - start_time))

    


    
    