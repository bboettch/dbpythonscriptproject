import cx_Oracle
from cx_Oracle import Error
import csv
import pandas.io.sql as psql
import random
import string
from faker import Faker
import time

def create_connection():            
    conn = None
    try:
        dsn = cx_Oracle.makedsn("student.canlgkvkulnk.us-west-2.rds.amazonaws.com", "1521", "ORCL")
        conn = cx_Oracle.connect(user = "admin", password = "4KkghduiQui8Gb7", dsn=dsn)
        return conn
    except Error as e:              #if no connection throw error
        print(e)
    
    return conn


#loop that load tests the database with update, insert, and query statements
def testLoop():

    fake = Faker()
    conn = create_connection()
    c = conn.cursor()

    #-------setting the percentages-------

    totalRecords = 10000
    insertPercentage = .40
    updatePercentage = .50
    queryPercentage = .10

    insertRange = int(totalRecords * insertPercentage)
    updateRange = int(totalRecords * updatePercentage)
    queryRange = int(totalRecords * queryPercentage)

    #------------inserts-------------

    for x in range(insertRange):
        #random value to decide which insert statement will be used
        val = random.randint(1,5)

        #temp values to build lists for new entries
        tmpDoctorId = random.randint(1, 10000)
        tmpLocationId = random.randint(1,10000)

        tmpOwner = random.randint(1, 10000)
        tmpAge = random.randint(1,120)


        if val == 1:
            #insert new location
        	locationList = [fake.company(), fake.phone_number(), fake.address()]
        	insert1 = "insert into location (name, phone, address) values (:name, :phone, :address)"
        	c.execute(insert1, [locationList[0], locationList[1], locationList[2]])

        elif val == 2:
            #insert new doctor
        	doctorList = [fake.name(), 'specialization', fake.phone_number(), fake.address()]
        	insert2 = "insert into doctor (name, specialization, phone, address) values (:name, :specialization, :phone, :address)"
        	c.execute(insert2, [doctorList[0], doctorList[1], doctorList[2], doctorList[3]])
          
        elif val == 3:
            #insert new meds
        	medList = ['name', fake.company(), '--mg', fake.name()]
        	insert3 = 'insert into medication(name, manufactorer, dose, prescribed_by)' 'values(:name, :manufactorer, :dose, :prescribed_by)'
        	c.execute(insert3, [medList[0], medList[1], medList[2], medList[3]])

        elif val == 4:
            #add a new visit record
            visitList = ['date', tmpDoctorId, tmpLocationId, 'treatment']
            insert4 = "insert into visit(datee, doctor_id, location_id, treatment) values (:datee, :doctor_id, :location_id, :treatment)"
            c.execute(insert4, [visitList[0], visitList[1], visitList[2], visitList[3]])

        else:
            #insert new pet
            petList = [tmpOwner, fake.name(), tmpAge, 'weight', 'species']
            insert5 = "insert into pet(owner_id, name, age, weight, species) values (:owner_id, :name, :age, :weight, :species)"
            c.execute(insert5, [petList[0], petList[1], petList[2], petList[3], petList[4]])

    conn.commit()

    #------------update-----------------

    for y in range(updateRange):
        #random value to decide which update statement to do.
        tmpVal = random.randint(1,5)
        
        #temp values used to build lists for the queries
        tmpName = fake.name()
        tmpAddr = fake.address()
        tmpNum = random.randint(1, 120)
        #print(tmpNum)
        tmpId = random.randint(1, 10000)
        #print(tmpId)
        tmpVar = [tmpNum, tmpId]

        #temp list used in changing address of owner
        tmpUpdate3 = [tmpAddr, tmpId]

        #temp list used in changing a test result
        update4List = ['newResult', tmpId]

        #changing weight of pet
        if tmpVal == 1:
            update1 = "UPDATE pet SET weight = :weight where pet_id = :pet_id"
            c.execute(update1,[tmpVar[0], tmpVar[1]])

        #changing policy number
        elif tmpVal == 2:
            update2 = "UPDATE insurance SET policy_number = :num WHERE insurance_id = :id"
            c.execute(update2, [tmpVar[0], tmpVar[1]])
        
        #changing address of owner
        elif tmpVal == 3:
            update3 = "UPDATE owner SET address = :address WHERE owner_id = :id"
            c.execute(update3, [tmpUpdate3[0], tmpUpdate3[1]])

        #changing test result from a test administered by doctor
        elif tmpVal == 4:
            update4 = "UPDATE test set result = :result WHERE test_id = :id"
            c.execute(update4, [update4List[0], update4List[1]])

        else: #changing age of pet
            update5 = "UPDATE pet set age = :age WHERE pet_id = :id"
            c.execute(update5, [tmpVar[0], tmpVar[1]])

    conn.commit()

    #--------------query----------------

    for z in range(queryRange):
        tmpChoice = random.randint(1,5)
        tempId = random.randint(1, 10000)

        if tmpChoice == 1:
            #querying list of pets given a customer_id (for uniqueness, use ID)
            query1 = 'SELECT name FROM pet WHERE owner_id = :id'
            c.execute(query1, [tempId])

        elif tmpChoice == 2:
            #getting heathhistory of specific pet_id
            query2 = 'SELECT healthhistory_id FROM healthhistory WHERE pet_id = :id'
            c.execute(query2, [tempId])
        
        elif tmpChoice == 3:
            #query what tests had been administered to specific pet
            query3 = 'SELECT test_id FROM healthhistory WHERE pet_id = :id'
            c.execute(query3, [tempId])

        elif tmpChoice == 4:
            #query how many visits per location site
            query4 = 'SELECT count(*) FROM visit WHERE location_id = :id'
            c.execute(query4, [tempId])

        else:
            #query insurance id of an owner
            query5 = 'SELECT insurance_id from owner WHERE owner_id = :id'
            c.execute(query5, [tempId])

    conn.commit()


def delete():               #deletes tables, columns, etc. from the database
    conn = None             # dangerous function but for the purposes of testing was useful to use

    delete_tables = ("DROP TABLE doctor CASCADE CONSTRAINTS",
                    "DROP TABLE healthhistory CASCADE CONSTRAINTS",
                    "DROP TABLE insurance CASCADE CONSTRAINTS",
                    "DROP TABLE location CASCADE CONSTRAINTS",
                    "DROP TABLE medication CASCADE CONSTRAINTS",
                    "DROP TABLE owner CASCADE CONSTRAINTS",
                    "DROP TABLE pet CASCADE CONSTRAINTS",
                    "DROP TABLE test CASCADE CONSTRAINTS",
                    "DROP TABLE visit CASCADE CONSTRAINTS",
    )

    try:
        conn = create_connection()      #creates connection
        c = conn.cursor()               #creates cursor

        for command in delete_tables:   #loops through delete statements dropping a table and all the tables needed to make it
            c.execute(command)
        print("All tables deleted")

        conn.commit()

        c.close()
    except (Exception, cx_Oracle.DatabaseError) as error:
        print("All not tables deleted")
        print(error)
    finally:
        if conn is not None:
            conn.close()


if __name__ == '__main__':
    start_time = time.time()    #used to time the execution time of script
    fake = Faker()

    testLoop()
    #delete()



    print("----- %s seconds -----" % (time.time() - start_time)) #prints time taken for execution

