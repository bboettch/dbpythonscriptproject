
#This script tests a series of connections to pinpoint network errors between clients, DNS,
#Oracle LDAP, DB Servers, and DB Storage.

import socket
from time import time 
from timeit import default_timer as timer
import urllib
import requests
import cx_Oracle
import sys

from PollyReports import *
from reportlab.pdfgen.canvas import Canvas
from pexpect import pxssh
import paramiko
 
import urllib.request as urllib2
import subprocess, sys
import os

import json

"""
Software Requirements to Run:
    Python 3

Goal:
 -> Check the runtime of the DNS servers
   
 -> Check the load time and result (status code) of any inputted website

 -> Create and output a summary of all of the above

"""

f = open('input.json')      #for JSON configuration file
data = json.load(f)

n = len(sys.argv)       #for command line inputs to distinguish local or remote connection


# initialize summaries of all parts
summaryA = []
summaryAA = []
summaryB = []
summaryC = []
summaryE = []
summaryF = []
summaryG = []
summaryI = []
summaryJ = []
summaryM = []
summaryN = []
completedRemote = 0       # accounts for multiple of same CL inputs like -r -r
completedLocal = 0       # accounts for multiple of same CL inputs like -l -l -l

if (len(sys.argv) == 1):
    print("Usage: scriptToExecute -[flag] -[flag]\n")
    print("Flags:   -r  -do a remote connection test")
    print("         -l  -do a local connection test (script must be executed in Database Server/Host")


for value in sys.argv:
    if (value == '-r' and completedRemote == 0):

        #----------- A: Check DNS runtime -----------#

        # array of DNS servers

        # loop through servers
        summaryA.append(["Server Name", "Runtime (sec)", "IP Address"])
        for server in data["servers"]:
            # start time
            dns_start = time()

            # get IP address from current DNS server
            ip_address = socket.gethostbyname(server)

            # stop time and calculate runtime
            dns_end = time()

            runtime = (dns_end - dns_start)

            # print information
            #print('DNS time for ', server, ' = ', runtime, ' ms')

            # add to summary
            line1 = server
            line2 = str(runtime)
            line3 = ip_address
            summaryA.append([line1, line2, line3])
            

        #------------ B/C/D: Check client site's ------------#

            # get website name from JSON file
        summaryAA.append(["Website Name", "Runtime (sec)", "Status Code"])
        for vip in data["vip"]:
            url = 'http://' + vip
            #print(url)

                # try to call a get request on the user input
                # handles invalid input (spelling errors, not a real site)
            try:
                req = requests.get(url)
                seconds = req.elapsed.total_seconds()
                code = req.status_code
            
            except:
                print('Did not recognize' + url, '. Try again.')


                # print information

                
                # add to summary
            line1 = url
            line2 = str(seconds)
            line3 = code
            summaryAA.append([line1, line2, line3])

        #static content check
        #staticUrl = data["nodes"][0] + data["static"]
        summaryB.append(["Static URL", "Runtime (sec)", "Status Code"])
        for staticUrl in data["static"]:
            static = requests.get(staticUrl)
            secs = static.elapsed.total_seconds()
            code2 = static.status_code

            #add static content to summary
            line1 = staticUrl
            line2 = str(secs)
            line3 = code2
            summaryB.append([line1, line2, line3])

        #dynamic content check
        summaryC.append(["Dynamic URL", "Runtime (sec)", "Status Code"])
        for dynamicUrl in data["dynamic"]:
            dynamic = requests.get(dynamicUrl)
            time = dynamic.elapsed.total_seconds()
            code3 = dynamic.status_code

            #add dyamic content to summary
            line1 = dynamicUrl
            line2 = str(time)
            line3 = code3
            summaryC.append([line1, line2, line3])

        ############ PART E ################
        #Takes oracle LDAP listed in the JSON file and uses socket to get the IP address and load time.

        #get TNS VIP IP and time elapsed
        hostname = data["tnsnamevip"]
        #for some reason, the time.time wouldn't work or was complicated to fix so I changed to using timer().
        tnsNameVipIpStartTime = timer()
        tnsNameVipIp = socket.gethostbyname(hostname)
        #tnsNameVipIpTime = tnsNameVipIp.elapsed.total_seconds()
        tnsNameVipIpEndTime = timer()
        #not really sure if we still need to multiply by 1000 anymore lol 
        tnsNameVipIpRunTime = (tnsNameVipIpEndTime - tnsNameVipIpStartTime)

        summaryE.append(["TNS VIP Name", "TNS VIP IP Address", "Runtime (sec)"])

        #add tnsnameVIP info into summary
        line1 = data["tnsnamevip"]
        line2 = tnsNameVipIp
        line3 = str(tnsNameVipIpRunTime)
        summaryE.append([line1, line2, line3])

        # part F,G,H : run the LDAP query to get the host: DONE
        # then an LDAP query to that VIP to translate the connection string to a host:port:sid, DONE
        # then DNS to get the IP of that (database) host, 
        # then SQL*Net to connect.

        #------------------PART F --------------------
        #Make sure the VIP was received and performed a NAT 


        #save the bash command into a variable which checks for the NAT performance
        ldapsearchVip = 'ldapsearch -x -h ' + data["tnsnamevip"] + ' -b "cn=SIS_TEST,cn=OracleContext,dc=ucdavis,dc=edu" -s sub "(|(objectclass=orclNetService)(objectclass=orclService))" orclnetdescstring "(|(objectclass=orclNetService)(objectclass=orclNetServiceAlias))" orclnetdescstring aliasedobjectname'
        #run the command
        cmd = subprocess.Popen(ldapsearchVip, shell = 'TRUE', stdout=subprocess.PIPE)
        #save the output into a string 
        output = str(cmd.communicate())

        output = output.split('\n')
        #splitting output so we can parse line by line
        output = output[0].split("(")

        #array to store the output
        res = []

        #iterate through output line by line, getting rid of the unnecessary characters we don't need.
        for line in output:
            line = line.replace("#", '')
            line = line.replace("\\n", '')
            res.append(line)

        #get the length of the output
        lengthOfOutput = len(res)

        #----------------------get the host, port, and sid of VIP after NAT----------------------
        #singling out the hostname
        hostName = res[lengthOfOutput - 5]
        hostName = hostName.replace(')', '')
        hostName = hostName.replace(hostName[:5], '')

        #singling out the port number by replacing the unncessary characters by a blank space
        portNum = res[lengthOfOutput - 4]
        portNum = portNum.replace(')', '')
        portNum = portNum.replace(portNum[:5], '')

        #singling out the sid by replacing the unnecessary characters by a blank space
        sid = res[lengthOfOutput - 2]
        sid = sid.replace(')', '')
        sid = sid.replace(sid[:4], '')

        #create a new string type list that saves the three things 
        vipServerInfoStr = (hostName, portNum, sid)
        #print(vipServerInfoStr[0])

        #add tnsnamevip host:port:sid info into summary
        summaryF.append(["TNS VIP Host", "TNS VIP Port", "TNS VIP SID"])
        line1 = vipServerInfoStr[0]
        line2 = vipServerInfoStr[1]
        line3 = vipServerInfoStr[2]
        summaryF.append([line1, line2, line3])

        #get the length of the tns nodes --> how many times to run the ldap search for each sub-ldap server 
        #Explanation: previous was the oraldap main one and the following are oracle-ldap1, ldap2, :servers that have been 
        #pulled up for testing or bc we need more servers
        lenOfTnsNodes = len(data["tnsnamenodes"])
        tnsNameNodeStr = [None] * lenOfTnsNodes

        #--------------------------- PART G ---------------------------------#
        #--------------- LDAP SEARCH FOR THE TNSNAME NODES ------------------#

        summaryG.append(["Node Name", "Node Host", "Port", "SID"])
        summaryI.append(["Node Name", "Node IP Address", "Runtime(sec)"])
        for i in range(0, lenOfTnsNodes):
            ldapsearchVip = 'ldapsearch -x -h ' + data["tnsnamenodes"][i] + ' -b "cn=SIS_TEST,cn=OracleContext,dc=ucdavis,dc=edu" -s sub "(|(objectclass=orclNetService)(objectclass=orclService))" orclnetdescstring "(|(objectclass=orclNetService)(objectclass=orclNetServiceAlias))" orclnetdescstring aliasedobjectname'
            cmd = subprocess.Popen(ldapsearchVip, shell = 'TRUE', stdout=subprocess.PIPE)
            output = str(cmd.communicate())

            #splitting output so we can parse line by line
            output = output.split('\n')
            output = output[0].split("(")

            #empty array to store parsed output
            res = []

            #iterate through output line by line
            for line in output:
                line = line.replace("#", '')
                line = line.replace("\\n", '')
                res.append(line)

            #print the output
            #for i in range(1, len(res) - 1):
            #   print(res[i])

            lengthOfOutput = len(res)
            #print(lengthOfOutput)

            #get the host, port, and sid
            #same as before where you go through saved output, get rid of the unnecessary characters
            hostName = res[lengthOfOutput - 5]
            hostName = hostName.replace(')', '')
            hostName = hostName.replace(hostName[:5], '')

            portNum = res[lengthOfOutput - 4]
            portNum = portNum.replace(')', '')
            portNum = portNum.replace(portNum[:5], '')

            sid = res[lengthOfOutput - 2]
            sid = sid.replace(')', '')
            sid = sid.replace(sid[:4], '')  
            #the rstrip gets rid of the empty space and whatever chracters are there starting from the right side
            sid.rstrip()

            #tnsNameNodeStr.append(hostName, portNum, sid)
            tnsNameNodeStr[i] = (hostName, portNum, sid)

            #add tnsnamenode host:port:sid info into summary
            line1 = data["tnsnamenodes"][i]
            line2 = tnsNameNodeStr[i][0]
            line3 = tnsNameNodeStr[i][1]
            line4 = tnsNameNodeStr[i][2]
            summaryG.append([line1, line2, line3, line4])

            #--------- RUNNING THE DIG COMMAND TO GET IP AND QUERY TIME OF EACH TNS NODE ------#
            #-------------------------------- Part I ------------------------------------------#

            tnsNodesIpCmd = 'dig ' + data["tnsnamenodes"][i] + ' +noall' + ' +answer' + ' +stats'
            cmd = subprocess.Popen(tnsNodesIpCmd, shell = 'TRUE', stdout=subprocess.PIPE)
            output = str(cmd.communicate())

            output = output.split('\n')
            #print(output[0])
            output = output[0].split(';')
            
            res = []

        #iterate through output line by line
            for line in output:
                line = line.replace("\\n", ' ')
                line = line.replace("\\t", ' ')
                #print(line)
                res.append(line)
                
            lenOfDigOutput = len(res)

            queryTime = res[lenOfDigOutput-7]

            extractIP = res[lenOfDigOutput-9][-15:]

            #add tnsnamenodes ip and query time info into summary
            line1 = data["tnsnamenodes"][i] 
            line2 = extractIP
            line3 = queryTime
        
            summaryI.append([line1, line2, line3])

        #sqlNetCmd = '(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=' + tnsNameNodeStr[i][0] + ')(PORT=' + tnsNameNodeStr[i][1] + '))(CONNECT_DATA=(SID=' + tnsNameNodeStr[i][2] + ')))'

        #echo "exit" | sqlplus -L uid/pwd@dbname | grep Connected > /dev/null
        #exit assures program exits immediately
        #-L assures sqlplus won't ask for password if credentials are not ok 
        #dev/null hides output from grep 

        #Usually would be checking credentials of the dbs found here, but for testing purposes, just
        #use rds where we have access


        #------------------------PART J---------------------------
        try:
            sqlplusCmd = 'echo "exit" | sqlplus -L ' + "'" + data["dbIdPwd"][0] + '/' + data["dbIdPwd"][1] + '@' + '(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=' + data["dbIdPwd"][2] + ')(PORT=' + data["dbIdPwd"][3] + '))(CONNECT_DATA=(SID=' + data["dbIdPwd"][4] + ")))'" + ' | grep Connected > /dev/null'
            startSqlCmd = timer()
            cmd = subprocess.Popen(sqlplusCmd, shell = 'TRUE', stdout=subprocess.PIPE)
            output = cmd.communicate()
            elapsed = timer()-startSqlCmd
            #print(output)
            
            if cmd.returncode == 0:
                #print(sqlOutput)
                sqlOutput = 'Succeeded'

        except subprocess.CalledProcessError:
            sqlOutput = 'Failed: error in sql connection statement'

        summaryJ.append(["Database User", "SQL Connection Status", "Runtime(sec)"])
        line1 = data["dbIdPwd"][0]
        line2 = sqlOutput
        line3 = str(elapsed)
        summaryJ.append([line1, line2, line3])

        completedRemote = 1




for value in sys.argv:
    if (value == '-l' and completedLocal == 0):

        # ----------- Part M will use sqlplus sys as sysdba statement or similar ----------- 

        # By default skip parts m and n unless there is a flag that is passed through to the script
        startSqlCmd = timer()
        summaryM.append(["Database User", "SQL Connection Status", "Runtime(sec)"])
        login = subprocess.Popen('sqlplus /nolog', shell = 'TRUE', stdout=subprocess.PIPE)
        output = cmd.communicate()
        print(output)
        print("\n\n\n")

        exitStatement = 'exit'
        logout = subprocess.Popen(exitStatement, shell = 'TRUE', stdout=subprocess.PIPE)
        output= cmd.communicate()
        print(output)


        elapsed = timer()-startSqlCmd



        #-------------------------PART N1, N2, N3--------------------------------

        ##NEED TO ESTABLISH SSH CONNECTION HERE
        startStorageCmd = timer()

        try:
            #SSH using private key file
            #Gets private key from local computer
            private_key = paramiko.RSAKey.from_private_key_file(data["Server1DB"][1]) #Make this a variable in JSON

            #Start ssh session
            c = paramiko.SSHClient()
            c.set_missing_host_key_policy(paramiko.AutoAddPolicy())

            #Establishes ssh session using the variables
            c.connect(hostname = data["Server1DB"][3], username = data["Server1DB"][0], pkey = private_key)

            errorMsgDbStorage = "SSH successful and local storage accessible"
            
            #executes touch command to create file in directory
            try:
                c.exec_command(data["Server1DB"][2])
                
            except:
                errorMsgDbStorage = "SSH successful but unable to access storage location"


            #-----------Make case for when ssh is successful but touch is not------------

            #closes ssh connection
            c.close()
            

        except:
            errorMsgDbStorage = "Failed: error in ssh connection to database"

        storageAccessTime = timer()-startStorageCmd
        summaryN.append(["Access status to local storage of database", "Runtime(sec)"])
        line1 = errorMsgDbStorage
        line2 = str(storageAccessTime)
        summaryN.append([line1, line2])

        completedLocal = 1


#------------------------------- Printing Results -------------#



# print summary
#print()
if (len(sys.argv) > 1):
    print('========================================= Summary =========================================\n')
#for line in summary:
    #print(line[0] + '     ', line[1]  + '     ', str(line[2]) + '     ')


# SOME FINAL COMMENTS
# Summary can only take three columns so sometimes organization can be difficult; maybe a way to rearrange output?
# Right now, we use the rds db for testing, but if we can get access to the dbs from the ldap servers, we can change
# sql connection credentials 

dash = '-' * 120

for value in sys.argv:
    if (value == '-r'):

        print("IP Address of Clients\n")
        for i in range(len(summaryA)):
            if i == 0:
                print('{:<30s}{:<30s}{:<30s}'.format(summaryA[i][0], summaryA[i][1], summaryA[i][2]))
                print(dash)
            else:
                print('{:<30s}{:<30s}{:<30s}'.format(summaryA[i][0], summaryA[i][1], summaryA[i][2]))

        print("\n")

        for i in range(len(summaryAA)):
            if i == 0:
                print('{:<30s}{:<30s}{:<30s}'.format(summaryAA[i][0], summaryAA[i][1], summaryAA[i][2]))
                print(dash)
            else:
                print('{:<30s}{:<30s}{:<30}'.format(summaryAA[i][0], summaryAA[i][1], summaryAA[i][2]))

        print("\n")

        print("Static Content Loading\n")
        for i in range(len(summaryB)):
            if i == 0:
                print('{:<80s}{:<20s}{:<20s}'.format(summaryB[i][0], summaryB[i][1], summaryB[i][2]))
                print(dash)
            else:
                print('{:<80s}{:<20s}{:<20}'.format(summaryB[i][0], summaryB[i][1], summaryB[i][2]))

        print("\n")

        print("Dynamic Content Loading\n")
        for i in range(len(summaryC)):
            if i == 0:
                print('{:<80s}{:<20s}{:<20s}'.format(summaryC[i][0], summaryC[i][1], summaryC[i][2]))
                print(dash)
            else:
                print('{:<80s}{:<20s}{:<20}'.format(summaryC[i][0], summaryC[i][1], summaryC[i][2]))

        print("\n")

        print("IP Address of Oracle LDAP\n")
        for i in range(len(summaryE)):
            if i == 0:
                print('{:<30s}{:<30s}{:<30s}'.format(summaryE[i][0], summaryE[i][1], summaryE[i][2]))
                print(dash)
            else:
                print('{:<30s}{:<30s}{:<30}'.format(summaryE[i][0], summaryE[i][1], summaryE[i][2]))

        print("\n")

        print("VIP received and NAT performace\n")
        for i in range(len(summaryF)):
            if i == 0:
                print('{:<30s}{:<30s}{:<30s}'.format(summaryF[i][0], summaryF[i][1], summaryF[i][2]))
                print(dash)
            else:
                print('{:<30s}{:<30s}{:<30}'.format(summaryF[i][0], summaryF[i][1], summaryF[i][2]))

        print("\n")

        print("Server Identification\n")
        for i in range(len(summaryG)):
            if i == 0:
                print('{:<30s}{:<30s}{:<30s}{:<30s}'.format(summaryG[i][0], summaryG[i][1], summaryG[i][2], summaryG[i][3]))
                print(dash)
            else:
                print('{:<30s}{:<30s}{:<30}{:<30s}'.format(summaryG[i][0], summaryG[i][1], summaryG[i][2], summaryG[i][3]))

        print("\n")

        print("IP Address of DB\n")
        for i in range(len(summaryI)):
            if i == 0:
                print('{:<30s}{:<30s}{:<30s}'.format(summaryI[i][0], summaryI[i][1], summaryI[i][2]))
                print(dash)
            else:
                print('{:<30s}{:<30s}{:<30}'.format(summaryI[i][0], summaryI[i][1], summaryI[i][2]))

        print("\n")

        print("DB Access remotely\n")

        for i in range(len(summaryI)):
            if i == 0:
                print('{:<30s}{:<30s}{:<30s}'.format(summaryI[i][0], summaryI[i][1], summaryI[i][2]))
                print(dash)
            else:
                print('{:<30s}{:<30s}{:<30}'.format(summaryI[i][0], summaryI[i][1], summaryI[i][2]))

print("\n")

for value in sys.argv:
    if (value == '-l'):

        print("DB Access locally\n")
        for i in range(len(summaryM)):
            if i == 0:
                print('{:<30s}{:<50s}{:<30s}'.format(summaryM[i][0], summaryM[i][1], summaryM[i][2]))
                print(dash)
            else:
                print('{:<30s}{:<50s}{:<30}'.format(summaryM[i][0], summaryM[i][1], summaryM[i][2]))

        print("\n")

        print("Database Storage Location Access\n")
        for i in range(len(summaryN)):
            if i == 0:
                print('{:<60s}{:<30s}'.format(summaryN[i][0], summaryN[i][1]))
                print(dash)
            else:
                print('{:<60s}{:<30s}'.format(summaryN[i][0], summaryN[i][1]))








#TO DO:
#Format JSON file and script to hand multiple entries
#Clean up entire file
#Configure Parts A-L to execute if a remote flag is passed to the script. Configure parts M and N for a local flag
#Write out Part M which will be a simple sqlplus sys as sysdba statement
