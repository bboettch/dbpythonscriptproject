
#This script tests a series of connections to pinpoint network errors between clients, DNS,
#Oracle LDAP, DB Servers, and DB Storage.

import socket               #Allows sql command executions
from time import time       #For timing result purposes
from timeit import default_timer as timer
import urllib
import requests             #For url, static, and dynamic content checks
import sys
import subprocess, sys      #For command line arguments
import os
import json                 #For configuration file

"""
Software Requirements to Run:
    Python 3

Goal:
 -> Check the runtime of the DNS servers
   
 -> Check the load time and result (status code) of any inputted website

 -> Create and output a summary of all of the above

"""
default = 1
n = len(sys.argv)       #for command line inputs to distinguish local or remote connection

for i in range(1,n):            #uses the JSON file passed through command line
    if (sys.argv[i] == '-f'):
        f = open(sys.argv[i+1])
        data = json.load(f)
        default = 0
        break

if (default != 0):
    f = open('input.json')      #for default JSON configuration to be used if one is not passed in commandline
    data = json.load(f)



# initialize summaries of all parts. Parts will be appended then printed out at the end for the summaries
summary_a = []
summary_aa = []
summary_b = []
summary_c = []
summary_e = []
summary_f = []
summary_g = []
summary_i = []
summary_j = []
summary_m = []
summary_n = []
completed_remote = 0       # accounts for multiple of same Command Line inputs like -r -r
completed_local = 0       # accounts for multiple of same Command Line inputs like -l -l -l


#Usage message if no command line arguments are passed
if (len(sys.argv) == 1):
    print("Usage: scriptToExecute -[flag] -[flag]\n")
    print("Flags:   -r  -do a remote connection test")
    print("         -l  -do a local connection test (script must be executed in Database Server/Host)")
    print("         -f  $FILEPATH -use config file specified (no flag uses default config file)")


#Runs a remote check
for value in sys.argv:
    if (value == '-r' and completed_remote == 0):

        #----------- A: Check DNS runtime -----------#

        # loop through servers
        summary_a.append(["Server Name", "Runtime (sec)", "IP Address"]) #Headers of the summary
        for server in data["servers"]:
            # start time
            dns_start = time()

            # get IP address from current DNS server (if error occurs here the servers data in config file is outdated)
            ip_address = socket.gethostbyname(server)

            # stop time and calculate runtime
            dns_end = time()

            runtime = (dns_end - dns_start)

            # add to summary
            line1 = server
            line2 = str(runtime)
            line3 = ip_address
            summary_a.append([line1, line2, line3])
            

        #------------ B/C/D: Check client site's ------------#

        # get website name from JSON file
        summary_aa.append(["Website Name", "Runtime (sec)", "Status Code"])
        seconds = 0
        code = ''
        for vip in data["vip"]:
            url = 'http://' + vip

            # try to call a get request on the website
            # handles invalid input (spelling errors, not a real site, etc.)
            try:
                req = requests.get(url)
                seconds = req.elapsed.total_seconds()
                code = req.status_code
            
            except:
                print('Did not recognize' + url, '. Try again.')
                
            # add to summary
            line1 = url
            line2 = str(seconds)
            line3 = code
            summary_aa.append([line1, line2, line3])

        #static content check
        summary_b.append(["Static URL", "Runtime (sec)", "Status Code"])
        for static_url in data["static"]:
            static = requests.get(static_url)
            secs = static.elapsed.total_seconds()
            code2 = static.status_code

            #add static content to summary
            line1 = static_url
            line2 = str(secs)
            line3 = code2
            summary_b.append([line1, line2, line3])

        #dynamic content check
        summary_c.append(["Dynamic URL", "Runtime (sec)", "Status Code"])
        for dynamic_url in data["dynamic"]:
            dynamic = requests.get(dynamic_url)
            time = dynamic.elapsed.total_seconds()
            code3 = dynamic.status_code

            #add dyamic content to summary
            line1 = dynamic_url
            line2 = str(time)
            line3 = code3
            summary_c.append([line1, line2, line3])

        #--------------- PART E ---------------#
        #Takes oracle LDAP listed in the JSON file and uses socket to get the IP address and load time.

        #get TNS VIP IP and time elapsed
        hostname = data["tnsnamevip"]

        #Start timer
        tns_name_vip_ip_start_time = timer()

        #Get hostname
        tns_name_vip_ip = socket.gethostbyname(hostname)

        #End timer
        tns_name_vip_ip_end_time = timer()

        tns_name_vip_ip_run_time = (tns_name_vip_ip_end_time - tns_name_vip_ip_start_time)

        #Summary headers
        summary_e.append(["TNS VIP Name", "TNS VIP IP Address", "Runtime (sec)"])

        #add tnsnameVIP info into summary
        line1 = data["tnsnamevip"]
        line2 = tns_name_vip_ip
        line3 = str(tns_name_vip_ip_run_time)
        summary_e.append([line1, line2, line3])

        # part F,G,H : run the LDAP query to get the host: DONE
        # then an LDAP query to that VIP to translate the connection string to a host:port:sid, DONE
        # then DNS to get the IP of that (database) host, 
        # then SQL*Net to connect.

        #------------------PART F --------------------
        #Make sure the VIP was received and performed a NAT 


        #save the bash command into a variable which checks for the NAT performance
        ldap_search_vip = 'ldapsearch -x -h ' + data["tnsnamevip"] + ' -b "cn=SIS_TEST,cn=OracleContext,dc=ucdavis,dc=edu" -s sub "(|(objectclass=orclNetService)(objectclass=orclService))" orclnetdescstring "(|(objectclass=orclNetService)(objectclass=orclNetServiceAlias))" orclnetdescstring aliasedobjectname'

        #run the command
        cmd = subprocess.Popen(ldap_search_vip, shell = 'TRUE', stdout=subprocess.PIPE)

        #save the output into a string 
        output = str(cmd.communicate())

        output = output.split('\n')
        #splitting output so we can parse line by line

        output = output[0].split("(")

        #array to store the output
        res = []

        #iterate through output line by line, getting rid of the unnecessary characters we don't need.
        for line in output:
            line = line.replace("#", '')
            line = line.replace("\\n", '')
            res.append(line)

        #get the length of the output
        length_of_output = len(res)

        #----------------------get the host, port, and sid of VIP after NAT----------------------
        #singling out the hostname
        host_name = res[length_of_output - 5]
        host_name = host_name.replace(')', '')
        host_name = host_name.replace(host_name[:5], '')

        #singling out the port number by replacing the unncessary characters by a blank space
        port_num = res[length_of_output - 4]
        port_num = port_num.replace(')', '')
        port_num = port_num.replace(port_num[:5], '')

        #singling out the sid by replacing the unnecessary characters by a blank space
        sid = res[length_of_output - 2]
        sid = sid.replace(')', '')
        sid = sid.replace(sid[:4], '')

        #create a new string type list that saves the three things 
        vip_server_info_str = (host_name, port_num, sid)


        #add tnsnamevip host:port:sid info into summary
        summary_f.append(["TNS VIP Host", "TNS VIP Port", "TNS VIP SID"])
        line1 = vip_server_info_str[0]
        line2 = vip_server_info_str[1]
        line3 = vip_server_info_str[2]
        summary_f.append([line1, line2, line3])

        #get the length of the tns nodes --> how many times to run the ldap search for each sub-ldap server 
        #Explanation: previous was the oraldap main one and the following are oracle-ldap1, ldap2, :servers that have been 
        #pulled up for testing or bc we need more servers
        len_of_tns_nodes = len(data["tnsnamenodes"])
        tns_name_node_str = [None] * len_of_tns_nodes

        #--------------------------- PART G ---------------------------------#
        #--------------- LDAP SEARCH FOR THE TNSNAME NODES ------------------#

        summary_g.append(["Node Name", "Node Host", "Port", "SID"])
        summary_i.append(["Node Name", "Node IP Address", "Runtime(sec)"])
        for i in range(0, len_of_tns_nodes):
            ldap_search_vip = 'ldapsearch -x -h ' + data["tnsnamenodes"][i] + ' -b "cn=SIS_TEST,cn=OracleContext,dc=ucdavis,dc=edu" -s sub "(|(objectclass=orclNetService)(objectclass=orclService))" orclnetdescstring "(|(objectclass=orclNetService)(objectclass=orclNetServiceAlias))" orclnetdescstring aliasedobjectname'
            cmd = subprocess.Popen(ldap_search_vip, shell = 'TRUE', stdout=subprocess.PIPE)
            output = str(cmd.communicate())

            #splitting output so we can parse line by line
            output = output.split('\n')
            output = output[0].split("(")

            #empty array to store parsed output
            res = []

            #iterate through output line by line
            for line in output:
                line = line.replace("#", '')
                line = line.replace("\\n", '')
                res.append(line)

            length_of_output = len(res)
            #print(lengthOfOutput)

            #get the host, port, and sid
            #same as before where you go through saved output, get rid of the unnecessary characters
            host_name = res[length_of_output - 5]
            host_name = host_name.replace(')', '')
            host_name = host_name.replace(host_name[:5], '')

            port_num = res[length_of_output - 4]
            port_num = port_num.replace(')', '')
            port_num = port_num.replace(port_num[:5], '')

            sid = res[length_of_output - 2]
            sid = sid.replace(')', '')
            sid = sid.replace(sid[:4], '')  
            #the rstrip gets rid of the empty space and whatever chracters are there starting from the right side
            sid.rstrip()

            tns_name_node_str[i] = (host_name, port_num, sid)

            #add tnsnamenode host:port:sid info into summary
            line1 = data["tnsnamenodes"][i]
            line2 = tns_name_node_str[i][0]
            line3 = tns_name_node_str[i][1]
            line4 = tns_name_node_str[i][2]
            summary_g.append([line1, line2, line3, line4])

            #--------- RUNNING THE DIG COMMAND TO GET IP AND QUERY TIME OF EACH TNS NODE ------#
            #-------------------------------- Part I ------------------------------------------#

            tns_nodes_ip_cmd = 'dig ' + data["tnsnamenodes"][i] + ' +noall' + ' +answer' + ' +stats'
            cmd = subprocess.Popen(tns_nodes_ip_cmd, shell = 'TRUE', stdout=subprocess.PIPE)
            output = str(cmd.communicate())

            output = output.split('\n')
            output = output[0].split(';')
            
            res = []

            #iterate through output line by line
            for line in output:
                line = line.replace("\\n", ' ')
                line = line.replace("\\t", ' ')
                #print(line)
                res.append(line)
                
            len_of_dig_output = len(res)

            query_time = res[len_of_dig_output-7]

            extract_ip = res[len_of_dig_output-9][-15:]

            #add tnsnamenodes ip and query time info into summary
            line1 = data["tnsnamenodes"][i] 
            line2 = extract_ip
            line3 = query_time
        
            summary_i.append([line1, line2, line3])


        #------------------------PART J---------------------------

        #Usually would be checking credentials of the dbs found here, but for testing purposes, just
        #use rds where we have access

        sql_output = ''

    
        try:
            sqlplus_cmd = 'echo "exit" | sqlplus -L ' + "'" + data["dbIdPwd"][0] + '/' + data["dbIdPwd"][1] + '@' + '(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=' + data["dbIdPwd"][2] + ')(PORT=' + data["dbIdPwd"][3] + '))(CONNECT_DATA=(SID=' + data["dbIdPwd"][4] + ")))'" + ' | grep Connected > /dev/null'

            #exit assures program exits immediately
            #-L assures sqlplus won't ask for password if credentials are not ok 
            #dev/null hides output from grep 

            start_sql_cmd = timer()
            cmd = subprocess.Popen(sqlplus_cmd, shell = 'TRUE', stdout=subprocess.PIPE)
            output = cmd.communicate()
            elapsed = timer()-start_sql_cmd
            
            if cmd.returncode == 0:
                sql_output = 'Succeeded'

        except subprocess.CalledProcessError:
            sql_output = 'Failed: error in sql connection statement'

        summary_j.append(["Database User", "SQL Connection Status", "Runtime(sec)"])
        line1 = data["dbIdPwd"][0]
        line2 = sql_output
        line3 = str(elapsed)
        summary_j.append([line1, line2, line3])

        completed_remote = 1


#Perform a local check from within the database
for value in sys.argv:
    if (value == '-l' and completed_local == 0):

        # ----------- Part M will use sqlplus sys as sysdba statement or similar ----------- 

        # By default skip parts m and n unless there is a flag (-l) that is passed through to the script

        status = ''

        try:
            start_sql_cmd = timer()
            login = subprocess.Popen('echo exit | sqlplus /nolog', shell = 'TRUE', stdout=subprocess.PIPE)
            #store log in files in config file
            output = login.communicate()
            elapsed = timer()-start_sql_cmd
            status = "Succeeded"
        except:
            status = "login failed"


        summary_m.append(["Database User", "SQL Connection Status", "Runtime(sec)"])
        summary_m.append(['sqlplus /nolog', status, elapsed])



        #-------------------------PART N1, N2, N3--------------------------------

        error_msg_db_storage = ''
        start_storage_cmd = 0
            
        #executes touch command to create file in directory
        try:
            local_storage_cmd = data["Server1DB"][2]
            start_storage_cmd = timer()
            cmd = subprocess.Popen(local_storage_cmd, shell = 'TRUE', stdout=subprocess.PIPE)
            output = str(cmd.communicate())
            storage_access_time = timer() - start_storage_cmd
            if cmd.returncode == 0:
                error_msg_db_storage = "Successful"
            else:
                error_msg_db_storage = 'ERROR : cannot access storage'

                
        except:
            error_msg_db_storage = "Failed. Problem beyond accessiblility"


            
        storage_access_time = timer() - start_storage_cmd
        summary_n.append(["Access status to local storage of database", "Runtime(sec)"])
        line1 = error_msg_db_storage
        line2 = str(storage_access_time)
        summary_n.append([line1, line2])

        completed_local = 1


#--------------- Printing Results -------------#


# print summary
if (len(sys.argv) > 1):
    print('========================================= Summary =========================================\n')


#For headers to seperate the different parts of the checks
dash = '-' * 120

for value in sys.argv:
    if (value == '-r'):

        #prints title of the section
        print("IP Address of Clients\n")

        #Prints contents of array in table format with 30 spaces allocated for each column
        for i in range(len(summary_a)):
            if i == 0:
                print('{:<30s}{:<30s}{:<30s}'.format(summary_a[i][0], summary_a[i][1], summary_a[i][2]))
                print(dash) #First line will print the dashes to establish the header
            else:
                print('{:<30s}{:<30s}{:<30s}'.format(summary_a[i][0], summary_a[i][1], summary_a[i][2]))
                #prints rest of the array following the format

        print("\n")

        for i in range(len(summary_aa)):
            if i == 0:
                print('{:<30s}{:<30s}{:<30s}'.format(summary_aa[i][0], summary_aa[i][1], summary_aa[i][2]))
                print(dash)
            else:
                print('{:<30s}{:<30s}{:<30}'.format(summary_aa[i][0], summary_aa[i][1], summary_aa[i][2]))

        print("\n")

        print("Static Content Loading\n")
        for i in range(len(summary_b)):
            if i == 0:
                print('{:<80s}{:<20s}{:<20s}'.format(summary_b[i][0], summary_b[i][1], summary_b[i][2]))
                print(dash)
            else:
                print('{:<80s}{:<20s}{:<20}'.format(summary_b[i][0], summary_b[i][1], summary_b[i][2]))

        print("\n")

        print("Dynamic Content Loading\n")
        for i in range(len(summary_c)):
            if i == 0:
                print('{:<80s}{:<20s}{:<20s}'.format(summary_c[i][0], summary_c[i][1], summary_c[i][2]))
                print(dash)
            else:
                print('{:<80s}{:<20s}{:<20}'.format(summary_c[i][0], summary_c[i][1], summary_c[i][2]))

        print("\n")

        print("IP Address of Oracle LDAP\n")
        for i in range(len(summary_e)):
            if i == 0:
                print('{:<30s}{:<30s}{:<30s}'.format(summary_e[i][0], summary_e[i][1], summary_e[i][2]))
                print(dash)
            else:
                print('{:<30s}{:<30s}{:<30}'.format(summary_e[i][0], summary_e[i][1], summary_e[i][2]))

        print("\n")

        print("VIP received and NAT performace\n")
        for i in range(len(summary_f)):
            if i == 0:
                print('{:<30s}{:<30s}{:<30s}'.format(summary_f[i][0], summary_f[i][1], summary_f[i][2]))
                print(dash)
            else:
                print('{:<30s}{:<30s}{:<30}'.format(summary_f[i][0], summary_f[i][1], summary_f[i][2]))

        print("\n")

        print("Server Identification\n")
        for i in range(len(summary_g)):
            if i == 0:
                print('{:<30s}{:<30s}{:<30s}{:<30s}'.format(summary_g[i][0], summary_g[i][1], summary_g[i][2], summary_g[i][3]))
                print(dash)
            else:
                print('{:<30s}{:<30s}{:<30}{:<30s}'.format(summary_g[i][0], summary_g[i][1], summary_g[i][2], summary_g[i][3]))

        print("\n")

        print("IP Address of DB\n")
        for i in range(len(summary_i)):
            if i == 0:
                print('{:<30s}{:<30s}{:<30s}'.format(summary_i[i][0], summary_i[i][1], summary_i[i][2]))
                print(dash)
            else:
                print('{:<30s}{:<30s}{:<30}'.format(summary_i[i][0], summary_i[i][1], summary_i[i][2]))

        print("\n")

        print("DB Access remotely\n")

        for i in range(len(summary_i)):
            if i == 0:
                print('{:<30s}{:<30s}{:<30s}'.format(summary_i[i][0], summary_i[i][1], summary_i[i][2]))
                print(dash)
            else:
                print('{:<30s}{:<30s}{:<30}'.format(summary_i[i][0], summary_i[i][1], summary_i[i][2]))

print("\n")


for value in sys.argv:
    if (value == '-l'):

        print("DB Access locally\n")
        for i in range(len(summary_m)):
            if i == 0:
                print('{:<30s}{:<50s}{:<30s}'.format(summary_m[i][0], summary_m[i][1], summary_m[i][2]))
                print(dash)
            else:
                print('{:<30s}{:<50s}{:<30}'.format(summary_m[i][0], summary_m[i][1], summary_m[i][2]))

        print("\n")

        print("Database Storage Location Access\n")
        for i in range(len(summary_n)):
            if i == 0:
                print('{:<60s}{:<30s}'.format(summary_n[i][0], summary_n[i][1]))
                print(dash)
            else:
                print('{:<60s}{:<30s}'.format(summary_n[i][0], summary_n[i][1]))



# SOME FINAL COMMENTS
# Right now, we use the rds db for testing, but if we can get access to the dbs from the ldap servers, we can change
# sql connection credentials 



#TO DO:
#set up monitoring account in the instance and use that as the log in
