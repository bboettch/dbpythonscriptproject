CREATE Table healthhistory(
    name VARCHAR(10)
);
Create table doctor(
    name VARCHAR(10)
);
Create table location(
    name VARCHAR(10)
);
Create table medication(
    name VARCHAR(10)
);
Create table owners(
    name VARCHAR(10)
);
Create table pet(
    name VARCHAR(10)
);
Create table test(
    name VARCHAR(10)
);
Create table visit(
    name VARCHAR(10)
);
Create table insurance(
    name VARCHAR(10)
);
DROP TABLE INSURANCE CASCADE CONSTRAINTS;
DROP TABLE owners CASCADE CONSTRAINTS;
DROP TABLE pet CASCADE CONSTRAINTS;
DROP TABLE doctor CASCADE CONSTRAINTS;
DROP TABLE healthhistory CASCADE CONSTRAINTS;
DROP TABLE location CASCADE CONSTRAINTS;
DROP TABLE visit CASCADE CONSTRAINTS;
DROP TABLE test CASCADE CONSTRAINTS;
DROP TABLE medication CASCADE CONSTRAINTS;

CREATE TABLE visit (
    visit_id NUMBER GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1),
    PRIMARY KEY(visit_id),
    datee VARCHAR2(100)
);

CREATE TABLE visit (
            visit_id NUMBER GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1),
            datee VARCHAR2(100),
            doctor_id NUMBER,
            location_id NUMBER,
            treatment VARCHAR2(500),
            PRIMARY KEY(visit_id),
            FOREIGN KEY (doctor_id) REFERENCES doctor(doctor_id),
            FOREIGN KEY (location_id) REFERENCES location(location_id)
)


