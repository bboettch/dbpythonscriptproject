import cx_Oracle
from cx_Oracle import Error
import csv
import pandas.io.sql as psql
import random
import string
from faker import Faker
import time 

def create_connection():            #establishes connection with "healthtest" database
    conn = None
    try:
        dsn = cx_Oracle.makedsn("student.canlgkvkulnk.us-west-2.rds.amazonaws.com", "1521", "ORCL")
        conn = cx_Oracle.connect(user = "admin", password = "4Rr+b8}eF5M]VcC6", dsn=dsn)
        return conn
    except Error as e:              #if no connection throw error
        print(e)
    
    return conn 

def import_location():

#conn = None
# Predefine the memory areas to match the table definition
    conn = create_connection()
    c = conn.cursor()

    c.setinputsizes(None, 25)

# Adjust the batch size to meet your memory and performance requirements
    batch_size = 10000

    with open('/Users/sharonyjn/repos/dbpythonscriptproject/location.csv', 'r') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter='|')
        sql = "insert into location (name, phone, address) values (:1, :2, :3)"
        data = []
        for line in csv_reader:
            data.append((line[1], line[2], line[3]))
            if len(data) % batch_size == 0:
                c.executemany(sql, data)
                data = []
        if data:
            c.executemany(sql, data)
        conn.commit()

def import_test():

#conn = None
# Predefine the memory areas to match the table definition
    conn = create_connection()
    c = conn.cursor()

    c.setinputsizes(None, 25)

# Adjust the batch size to meet your memory and performance requirements
    batch_size = 10000

    with open('/Users/sharonyjn/repos/dbpythonscriptproject/test.csv', 'r') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter='|')
        sql = "insert into test (name, result, ordered_by) values (:1, :2, :3)"
        data = []
        for line in csv_reader:
            data.append((line[1], line[2], line[3]))
            if len(data) % batch_size == 0:
                c.executemany(sql, data)
                data = []
        if data:
            c.executemany(sql, data)
        conn.commit()


def import_doctor():

#conn = None
# Predefine the memory areas to match the table definition
    conn = create_connection()
    c = conn.cursor()

    c.setinputsizes(None, 25)

# Adjust the batch size to meet your memory and performance requirements
    batch_size = 10000

    with open('/Users/sharonyjn/repos/dbpythonscriptproject/doctor.csv', 'r') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter='|')
        sql = "insert into doctor (name, specialization, phone, address) values (:1, :2, :3, :4)"
        data = []
        for line in csv_reader:
            data.append((line[1], line[2], line[3], line[4]))
            if len(data) % batch_size == 0:
                c.executemany(sql, data)
                data = []
        if data:
            c.executemany(sql, data)
        conn.commit()

def import_healthhistory():

#conn = None
# Predefine the memory areas to match the table definition
    conn = create_connection()
    c = conn.cursor()

    c.setinputsizes(None, 25)

# Adjust the batch size to meet your memory and performance requirements
    batch_size = 10000

    with open('/Users/sharonyjn/repos/dbpythonscriptproject/healthhistory.csv', 'r') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter='|')
        sql = "insert into healthhistory (pet_id, visit_id, medication_id, test_id) values (:1, :2, :3, :4)"
        data = []
        for line in csv_reader:
            data.append((line[1], line[2], line[3], line[4]))
            if len(data) % batch_size == 0:
                c.executemany(sql, data)
                data = []
        if data:
            c.executemany(sql, data)
        conn.commit()

def import_insurance():

#conn = None
# Predefine the memory areas to match the table definition
    conn = create_connection()
    c = conn.cursor()

    c.setinputsizes(None, 25)

# Adjust the batch size to meet your memory and performance requirements
    batch_size = 10000

    with open('/Users/sharonyjn/repos/dbpythonscriptproject/insurance.csv', 'r') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter='|')
        sql = "insert into insurance (policy_number, provider, phone) values (:1, :2, :3)"
        data = []
        for line in csv_reader:
            data.append((line[1], line[2], line[3]))
            if len(data) % batch_size == 0:
                c.executemany(sql, data)
                data = []
        if data:
            c.executemany(sql, data)
        conn.commit()

def import_medication():

#conn = None
# Predefine the memory areas to match the table definition
    conn = create_connection()
    c = conn.cursor()

    c.setinputsizes(None, 25)

# Adjust the batch size to meet your memory and performance requirements
    batch_size = 10000

    with open('/Users/sharonyjn/repos/dbpythonscriptproject/medication.csv', 'r') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter='|')
        sql = "insert into medication (name, manufactorer, dose, prescribed_by) values (:1, :2, :3, :4)"
        data = []
        for line in csv_reader:
            data.append((line[1], line[2], line[3], line[4]))
            if len(data) % batch_size == 0:
                c.executemany(sql, data)
                data = []
        if data:
            c.executemany(sql, data)
        conn.commit()

def import_owner():

#conn = None
# Predefine the memory areas to match the table definition
    conn = create_connection()
    c = conn.cursor()

    c.setinputsizes(None, 25)

# Adjust the batch size to meet your memory and performance requirements
    batch_size = 10000

    with open('/Users/sharonyjn/repos/dbpythonscriptproject/owner.csv', 'r') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter='|')
        sql = "insert into owner (insurance_id, name, phone, address) values (:1, :2, :3, :4)"
        data = []
        for line in csv_reader:
            data.append((line[1], line[2], line[3], line[4]))
            if len(data) % batch_size == 0:
                c.executemany(sql, data)
                data = []
        if data:
            c.executemany(sql, data)
        conn.commit()

def import_pet():

#conn = None
# Predefine the memory areas to match the table definition
    conn = create_connection()
    c = conn.cursor()

    c.setinputsizes(None, 25)

# Adjust the batch size to meet your memory and performance requirements
    batch_size = 10000

    with open('/Users/sharonyjn/repos/dbpythonscriptproject/pet.csv', 'r') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter='|')
        sql = "insert into pet (owner_id, name, age, weight, species) values (:1, :2, :3, :4, :5)"
        data = []
        for line in csv_reader:
            data.append((line[1], line[2], line[3], line[4], line[5]))
            if len(data) % batch_size == 0:
                c.executemany(sql, data)
                data = []
        if data:
            c.executemany(sql, data)
        conn.commit()

def import_visit():

#conn = None
# Predefine the memory areas to match the table definition
    conn = create_connection()
    c = conn.cursor()

    c.setinputsizes(None, 25)

# Adjust the batch size to meet your memory and performance requirements
    batch_size = 10000

    with open('/Users/sharonyjn/repos/dbpythonscriptproject/visit.csv', 'r') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter='|')
        sql = "insert into visit (datee, doctor_id, location_id, treatment) values (:1, :2, :3, :4)"
        data = []
        for line in csv_reader:
            data.append((line[1], line[2], line[3], line[4]))
            if len(data) % batch_size == 0:
                c.executemany(sql, data)
                data = []
        if data:
            c.executemany(sql, data)
        conn.commit()

if __name__ == '__main__':
    start_time = time.time()

    #create_connection() 
    import_insurance()
    import_owner()
    import_pet()
    import_doctor()
    import_location()        #Create CSV to feed in and enters them
    import_test()
    import_medication()
    import_visit()
    import_healthhistory()
    





    #create_baseline(10)
    #create_records(1000)
    #delete()                   #Uncomment if you want to delete all tables and re-enter fields

    print("----- %s seconds -----" % (time.time() - start_time))